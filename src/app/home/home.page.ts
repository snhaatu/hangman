import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  words = [] ;
  guessed: string;
  guess: string;
  hidden_word: string;
  word_to_be_guessed: string;

  constructor(private toastController: ToastController) {}

  ngOnInit() {
    // Add words that user tries to guess into the array.
    this.words.push('mobile');
    this.words.push('programming');
    this.words.push('object-orientation');

    // Randomize word and assign to variables.
    const random_number = this.random();
    this.word_to_be_guessed = this.words[random_number];
    const star: string = '*';
    this.hidden_word = star.repeat(this.word_to_be_guessed.length);
  }

  //Method return random number between 0 and size of array containing words to be guessed.
  random(): number {
    const rand = Math.floor(Math.random() *this.words.length);
    return rand;
  }

  //Method check user's answer.
 async check(event) {
    // If enter is pressed and there is atleast one character.
    if(event.keyCode === 13 && this.guess.length > 0) {
      if(this.guess.length === 1) { // One Character guessed.
        if(this.guessed != undefined) {
          this.guessed = this.guessed + ',' + this.guess;
        }else {
          this.guessed = this.guess;
        }


      for (let i = 0;i< this.word_to_be_guessed.length;i++) {
        if(this.word_to_be_guessed.substr(i,1) === this.guess) {
          this.hidden_word = this.hidden_word.substring(0, i) + this.guess + this.hidden_word.substring(i + 1);
        }
      }

      }else {  // User tries to guess the whole world
        if(this.guess === this.word_to_be_guessed) {
          this.hidden_word = this.word_to_be_guessed;
        }
      }

      if (this.hidden_word === this.word_to_be_guessed) {
        const toast = await this.toastController.create({
          message: "You have guessed the word",
          position: "top",
          duration: 3000
        });
        toast.present();
      }

      this.guess = '';
    }
  }

}
